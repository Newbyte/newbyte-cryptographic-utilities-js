export declare class CryptoUtils {
    private readonly text_encoder;
    private readonly lut;
    constructor(text_encoder: TextEncoder);
    double_SHA256_string_salted(password: string, salt: string): Promise<string>;
    SHA256(string_to_hash: string): Promise<Uint8Array>;
    SHA256_string(password: string): Promise<string>;
    generate_salt(): string;
    generate_UUID(): string;
}
export default CryptoUtils;
