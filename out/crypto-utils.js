var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
export class CryptoUtils {
    constructor(text_encoder) {
        this.text_encoder = text_encoder;
        this.lut = [];
        for (let i = 0; i < 256; i++) {
            this.lut[i] = (i < 16 ? '0' : '') + (i).toString(16);
        }
    }
    double_SHA256_string_salted(password, salt) {
        return __awaiter(this, void 0, void 0, function* () {
            const password_hash = yield this.SHA256_string(password);
            const salted_hash = password_hash + salt;
            return yield this.SHA256_string(salted_hash);
        });
    }
    SHA256(string_to_hash) {
        return __awaiter(this, void 0, void 0, function* () {
            const password_buffer = this.text_encoder.encode(string_to_hash);
            const hash_buffer = yield crypto.subtle.digest("SHA-512", password_buffer);
            return new Uint8Array(hash_buffer);
        });
    }
    SHA256_string(password) {
        return __awaiter(this, void 0, void 0, function* () {
            const password_buffer = new TextEncoder().encode(password);
            const hash_buffer = yield crypto.subtle.digest("SHA-256", password_buffer);
            const hash_array = Array.from(new Uint8Array(hash_buffer));
            return hash_array
                .map(b => ("00" + b.toString(16))
                .slice(-2))
                .join("");
        });
    }
    generate_salt() {
        return Math.round(Math.random() * 100).toString();
    }
    generate_UUID() {
        const d0 = Math.random() * 0xffffffff | 0;
        const d1 = Math.random() * 0xffffffff | 0;
        const d2 = Math.random() * 0xffffffff | 0;
        const d3 = Math.random() * 0xffffffff | 0;
        return this.lut[d0 & 0xff] + this.lut[d0 >> 8 & 0xff] + this.lut[d0 >> 16 & 0xff] + this.lut[d0 >> 24 & 0xff] + '-' +
            this.lut[d1 & 0xff] + this.lut[d1 >> 8 & 0xff] + '-' + this.lut[d1 >> 16 & 0x0f | 0x40] + this.lut[d1 >> 24 & 0xff] + '-' +
            this.lut[d2 & 0x3f | 0x80] + this.lut[d2 >> 8 & 0xff] + '-' + this.lut[d2 >> 16 & 0xff] + this.lut[d2 >> 24 & 0xff] +
            this.lut[d3 & 0xff] + this.lut[d3 >> 8 & 0xff] + this.lut[d3 >> 16 & 0xff] + this.lut[d3 >> 24 & 0xff];
    }
}
export default CryptoUtils;
//# sourceMappingURL=crypto-utils.js.map