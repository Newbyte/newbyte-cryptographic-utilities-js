var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
export var HashAlgorithm;
(function (HashAlgorithm) {
    HashAlgorithm["SHA_256"] = "SHA-256";
    HashAlgorithm["SHA_512"] = "SHA-512";
})(HashAlgorithm || (HashAlgorithm = {}));
export class Hasher {
    constructor(text_encoder) {
        this.text_encoder = text_encoder;
    }
    double_hash_string_salted(string_to_hash, salt, algorithm) {
        return __awaiter(this, void 0, void 0, function* () {
            const string_hash = yield this.hash_string(string_to_hash, algorithm);
            const salted_hash = string_hash + salt;
            return yield this.hash_string(salted_hash, algorithm);
        });
    }
    hash(string_to_hash, algorithm) {
        return __awaiter(this, void 0, void 0, function* () {
            const string_buffer = this.text_encoder.encode(string_to_hash);
            const hash_buffer = yield crypto.subtle.digest(algorithm, string_buffer);
            return new Uint8Array(hash_buffer);
        });
    }
    hash_string(string_to_hash, algorithm) {
        return __awaiter(this, void 0, void 0, function* () {
            const string_buffer = new TextEncoder().encode(string_to_hash);
            const hash_buffer = yield crypto.subtle.digest(algorithm, string_buffer);
            const hash_array = Array.from(new Uint8Array(hash_buffer));
            return hash_array
                .map(b => ("00" + b.toString(16))
                .slice(-2))
                .join("");
        });
    }
}
export default Hasher;
//# sourceMappingURL=Hasher.js.map