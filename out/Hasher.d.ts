export declare enum HashAlgorithm {
    SHA_256 = "SHA-256",
    SHA_512 = "SHA-512"
}
export declare class Hasher {
    private readonly text_encoder;
    constructor(text_encoder: TextEncoder);
    double_hash_string_salted(string_to_hash: string, salt: string, algorithm: HashAlgorithm): Promise<string>;
    hash(string_to_hash: string, algorithm: HashAlgorithm): Promise<Uint8Array>;
    hash_string(string_to_hash: string, algorithm: HashAlgorithm): Promise<string>;
}
export default Hasher;
