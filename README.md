# Newbyte's Cryptographic Utilities.js

Library I use for personal JavaScript projects. Codebase and available
 functionality is tiny, so read the files in src/ for information about
 what's available. Proper documentation will come.

# Installation

Package is available at npm and can be installed via the following command:

```sh
npm install @newbytee/crypto-utils
```

**Note that the double e in newbytee is intentional.**

# Links

npm registry entry: https://www.npmjs.com/package/@newbytee/crypto-utils

